一个低代码平台的前端部分，靠拖拉拽生成页面。
## 在线预览：
预览地址：[https://qkongtao.gitee.io/visual-drag-demo](https://qkongtao.gitee.io/visual-drag-demo)

## 功能点
这是本项目具有的功能点，如果想了解详情请参考本项目的四篇文档，每个功能点都有不同程度的描述以及动图帮助你理解。
1. 编辑器
2. 自定义组件（文本、图片、矩形、圆形、直线、星形、三角形、按钮、表格、组合）
3. 接口请求（通过接口请求组件数据）
4. 组件联动
5. 拖拽
6. 删除组件、调整图层层级
7. 放大缩小
8. 撤消、重做
9. 组件属性设置
10. 吸附
11. 预览、保存代码
12. 绑定事件
13. 绑定动画
14. 拖拽旋转
15. 复制粘贴剪切
16. 多个组件的组合和拆分
17. 锁定组件
18. 网格线

## 文档
* [可视化拖拽组件库一些技术要点原理分析](https://github.com/woai3c/Front-end-articles/issues/19)
* [可视化拖拽组件库一些技术要点原理分析（二）](https://github.com/woai3c/Front-end-articles/issues/20)
* [可视化拖拽组件库一些技术要点原理分析（三）](https://github.com/woai3c/Front-end-articles/issues/21)
* [可视化拖拽组件库一些技术要点原理分析（四）](https://github.com/woai3c/Front-end-articles/issues/33)

## 开发
### 安装
```
npm i
```
### 运行
```
npm run serve
```
### 打包
```
npm run build
```

